from django.urls import path
from .views import receipt_list, receipt_create, account_list, category_list, category_create, account_create
urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", receipt_create, name="create_receipt"),
    path("accounts/",account_list,name="account_list"),
    path("categories/",category_list,name="category_list"),
    path("categories/create/",category_create,name="create_category"),
    path("accounts/create/",account_create,name="create_account")

]
