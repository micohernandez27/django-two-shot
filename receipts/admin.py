from django.contrib import admin
from .models import Receipt, Account, ExpenseCategory
# Register your models here.
@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = ("vendor", "total", "tax", "date", "purchaser")
@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ("name", "number", "owner")
@admin.register(ExpenseCategory)
class ExpenseCategory(admin.ModelAdmin):
    list_display = ("name", "owner")
